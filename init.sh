#!/bin/bash -xe

SERVERS=
SSL="-t"

for HST in ${HOST_LIST}
do
   echo $SERVERS
   SERVERS=" -s /${HST}:SSH:${HST} ${SERVERS} "
done

export SERVERS
export SSL
cat /etc/supervisor/conf.d/shellinabox.conf.in | envsubst > /etc/supervisor/conf.d/shellinabox.conf

exec supervisord -n -c /etc/supervisor/supervisord.conf
