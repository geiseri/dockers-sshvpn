## Simple HTML5 to SSH VPN

This is just a small docker with shellinabox running.  It is meant to be run more as an application in the way that you can expose multiple hosts at the commandline.

Example:
```
$ docker run -d -p 4200:4200 --name sshvpn -e HOST_LIST="hostone.local hosttwo.local 10.0.5.9" geiseri/sshvpn
```

This would expose:

1. http://localhost:4200/hostone.local -> ssh prompt at hostone.local
1. http://localhost:4200/hosttwo.local -> ssh prompt at hosttwo.local
1. http://localhost:4200/10.0.5.9 -> ssh prompt at 10.0.5.9


