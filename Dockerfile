FROM debian:jessie
MAINTAINER Ian Geiser <geiseri@yahoo.com>

ENV DEBIAN_FRONTEND noninteractive

RUN mkdir /work && cd /work && \
    apt-get update -qy && \
    apt-get install -qy supervisor git libssl-dev libpam0g-dev zlib1g-dev dh-autoreconf \
                    nano gettext-base ssh-client && \
    git clone https://github.com/shellinabox/shellinabox.git && cd shellinabox && \
    dpkg-buildpackage -b && \
    dpkg -i ../shellinabox_2.15-1_amd64.deb && \
    apt-get remove -qy --purge git libssl-dev libpam0g-dev zlib1g-dev dh-autoreconf && \
    apt-get autoremove -qy --purge && \ 
    apt-get clean -qy && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* && \
    cd / && rm -rf /work

# configure shellinabox
ADD shellinabox.conf.in /etc/supervisor/conf.d/shellinabox.conf.in
EXPOSE 4200

# configure entrypoint
ADD init.sh /init.sh
RUN chmod 755 /init.sh

ENTRYPOINT ["/init.sh"]


